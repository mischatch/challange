import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { shallow, mount, render, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('<App /> component',  () => {

  it('Renders without crashing', () => {
    const wrapper = shallow(<App />);
    expect(wrapper).toHaveLength(1);
  });

  it('componentWillMount fetches 10 sample users and changes state properly', () => {
    const wrapper = shallow(<App />);
    return wrapper.instance().componentWillMount().then(res => {
      expect(wrapper.state('filtred')).toHaveLength(10);
    });
 });
});
