import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount, render, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PrefPan from './components/prefPan';

configure({ adapter: new Adapter() });


describe('<PrefPan /> component', () => {

  it('Renders without crashing', () => {
    const div = document.createElement('div');
    const wrapper = shallow(<PrefPan />);
    expect(wrapper).toHaveLength(1);
  });

  test('Has "Any" button selected by default', () => {
    const wrapper = shallow(<PrefPan />);
    let anyButton = wrapper.find('input[type="radio"]').at(0);
    expect(anyButton.props().checked).toBe(true);
  });

  test('Radio selection changes after clicking in "Male" button', () => {
    const wrapper = shallow(<PrefPan />);
    wrapper.setState({ radio: 'male' });
    let anyButton = wrapper.find('input[type="radio"]').at(1);
    expect(anyButton.props().checked).toBe(true);
  });

  test('Radio selection changes after clicking in "Female" button', () => {
    const wrapper = shallow(<PrefPan />);
    wrapper.setState({ radio: 'female' });
    let anyButton = wrapper.find('input[type="radio"]').at(2);
    expect(anyButton.props().checked).toBe(true);
  });


});
