export const getRandom10 = () => {

  const request = new Request('https://randomuser.me/api/?results=10&nat=US', {
      method: 'GET',
      mode: 'cors',
      headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }),
    });

  return fetch(request)
    .then(res => {
      return res.json();
    })
    .catch(err => {
      console.log(err.message);
    });
};
