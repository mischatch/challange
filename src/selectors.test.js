import { getAge, capitalize } from './components/selectors';


describe('Selectors', () => {
  it('getAge returns exact age from date string', () => {
    const str = '1983-07-14 07:29:45';
    expect(getAge(str)).toBe(35);
  });

  it('capitalize returns capitalized name', () => {
    const name = 'john';
    expect(capitalize(name)).toBe('John');
  });
});
