import React from 'react';
import ReactDOM from 'react-dom';
import SearchResults from './components/searchResults';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('<SearchResults /> component', () => {
  it('Renders without crashing', () => {
    const props = [
      { gender: "male",
        name: {
          title: "mr",
          first: "romain",
          last: "hoogmoed"
        },
        picture: {
          large: "https://randomuser.me/api/portraits/men/83.jpg",
          medium: "https://randomuser.me/api/portraits/med/men/83.jpg",
          thumbnail: "https://randomuser.me/api/portraits/thumb/men/83.jpg"
        },
        dob: "1983-07-14 07:29:45",
        email: "romain.hoogmoed@example.com",
        phone: "(656)-976-4980",
        cell: "(065)-247-9303",
        nat: "NL"  },
        {
        gender: "male",
        name: {
          title: "mr",
          first: "romain",
          last: "hoogmoed"
        },
        picture: {
          large: "https://randomuser.me/api/portraits/men/83.jpg",
          medium: "https://randomuser.me/api/portraits/med/men/83.jpg",
          thumbnail: "https://randomuser.me/api/portraits/thumb/men/83.jpg"
        },
        dob: "1983-07-14 07:29:45",
        email: "romain.hoogmoed@example.com",
        phone: "(656)-976-4980",
        cell: "(065)-247-9303",
        nat: "NL"  },
        {
        gender: "male",
        name: {
          title: "mr",
          first: "romain",
          last: "hoogmoed"
        },
        picture: {
          large: "https://randomuser.me/api/portraits/men/83.jpg",
          medium: "https://randomuser.me/api/portraits/med/men/83.jpg",
          thumbnail: "https://randomuser.me/api/portraits/thumb/men/83.jpg"
        },
        dob: "1983-07-14 07:29:45",
        email: "romain.hoogmoed@example.com",
        phone: "(656)-976-4980",
        cell: "(065)-247-9303",
        nat: "NL"  }
      ];

    expect(shallow(<SearchResults data={props} />)).toHaveLength(1);
  });
});
