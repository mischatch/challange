import React, { Component } from "react";
import "../public/styles.css";
import PrefPan from './components/prefPan';
import SearchResults from './components/searchResults';
import { getRandom10 } from './searchUtil';
import { getAge, customStyles } from './components/selectors';
import Modal from 'react-modal';

class App extends Component{
  constructor(props){
    super(props);

    this.state = {
      search: [],
      filtred: [],
      modalOpen: false,
      modalInfo: {},
    };

    this.filter = this.filter.bind(this);
    this.reset = this.reset.bind(this);
    this.modalShow = this.modalShow.bind(this);
    this.modalClose = this.modalClose.bind(this);
  }

  componentWillMount(){
    return getRandom10()
      .then(res => {
        if (res){
          this.setState({
            search: res.results,
            filtred: res.results
          });
        }
      });
  }

  filter(params){
    const { ageMax, ageMin, radio } = params;
    const { search } = this.state;
    let filtred = [];
    search.forEach(item => {
      const { dob, gender } = item;
      const age = parseInt(getAge(dob));
      if(age >= parseInt(ageMin) && age <= parseInt(ageMax)){
        if(radio === gender || radio === 'any'){
          filtred.push(item);
        }
      }
    });
    this.setState({ filtred: filtred });
  }

  reset(){
    this.setState({
      filtred: this.state.search
    });
  }

  modalShow(data){
    this.setState({
      modalOpen: true,
      modalInfo: data
    });
  }

  modalClose(){
    this.setState({
      modalOpen: false,
      modalInfo: {},
    });
  }

  render(){
    const { filtred, modalInfo } = this.state;
    const { email, phone, cell } = modalInfo;

    return(
      <div className="App">
        <div className="header">
          <h1>Lonely Hearts Dating Service</h1>
        </div>

        <PrefPan filter={this.filter} reset={this.reset} />
        <SearchResults results={filtred} modalShow={this.modalShow} />

        <Modal
          contentLabel="Modal"
          ariaHideApp={false}
          isOpen={this.state.modalOpen}
          onRequestClose={this.modalClose}
          style={customStyles} >

          <div className='Modal-container'>
            <p>email: {email}</p>
            <p>phone: {phone}</p>
            <p>cell: {cell}</p>
          </div>

        </Modal>
      </div>
    );
  }
}

export default App;
