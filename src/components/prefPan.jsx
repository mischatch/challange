import React, { Component } from 'react';

class PrefPan extends Component {
  constructor(props){
    super(props);

    this.state = {
      ageMin: '',
      ageMax: '',
      radio: 'any'
    };

    this.handleInput = this.handleInput.bind(this);
    this.handleRadio = this.handleRadio.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }

  handleInput(e){
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleRadio(e){
    this.setState({
      radio: e.target.name
    });
  }

  handleSubmit(e){
    e.preventDefault();
    this.props.filter(this.state);
  }

  handleReset(e){
    this.setState({
      ageMin: '',
      ageMax: '',
      radio: 'any'
    });
    this.props.reset();
  }

  render(){
    const { radio } = this.state;

    return(
      <div className='pref-container'>
        <div id="userPreferences">
          <h2>Search Criteria</h2>
          <form className="form">
            <div className="form-group">
              <label className="control-label">Age</label>
              <input type="text" name="ageMin"
                placeholder='ex. 20'
                onChange={this.handleInput}
                value={this.state.ageMin} />
              <input type="text" name="ageMax"
                placeholder='ex. 60'
                onChange={this.handleInput}
                value={this.state.ageMax} />
            </div>

            <div className="form-group">
              <label className="control-label">Gender</label>
              <label className="radio-inline"><input type="radio"
                name="any"
                onChange={this.handleRadio}
                checked={radio === 'any'}
                />Any<span></span></label>
              <label className="radio-inline"><input type="radio"
                name="male"
                onChange={this.handleRadio}
                checked={radio === 'male'}
                />Male<span></span></label>
              <label className="radio-inline"><input type="radio"
                name="female"
                onChange={this.handleRadio}
                checked={radio === 'female'}
                />Female<span></span></label>
            </div>
            <div className='buttons'>
              <button type="button" className="btn btn-default"
                onClick={this.handleReset}
                >Reset</button>
              <button type="submit" className="btn btn-primary"
                onClick={this.handleSubmit}
                >Filter</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default PrefPan;
