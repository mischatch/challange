export const getAge = (dob) => {

  let today = new Date();
  let birthDate = new Date(dob.date.replace(/\s/, 'T'));
  let age = today.getFullYear() - birthDate.getFullYear();
  return age;
};

export const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

export const capitalize = (name) => {
  return name[0].toUpperCase() + name.slice(1).toLowerCase();
};
