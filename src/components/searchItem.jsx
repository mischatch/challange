import React, { Component } from 'react';
import { getAge, capitalize } from './selectors';

const SearchItem = (props) => {

    const { name, picture, dob, email, phone, cell } = props.data;
    let age = getAge(dob);

    const modal = () => {
      let data = { email, phone, cell };
      props.modalShow(data);
    };

    return(
      <li>
        <img className="photo" src={picture.large} />
        <div>
          <div>
            <span className="name lead">{capitalize(name.first)} {capitalize(name.last)}</span>
            <span className="age lead">{age}</span>
          </div>
          <button onClick={modal} >Contact</button>
        </div>
      </li>
    );
};


export default SearchItem;
