import React, { Component } from 'react';
import SearchItem from './searchItem';

class SearchResults extends Component {


  render(){
    const { results, modalShow } = this.props;
    if(!Boolean(results)){
      return (
        <div id="searchResults">
          <h2>Loading candidates ... </h2>
        </div>
      );
    } else {
      return(
        <div id="searchResults">
          <h2>{results.length} Candidates Found</h2>
          <ul>
            { results.map((item, idx) => <SearchItem key={idx} data={item} modalShow={modalShow} />) }
          </ul>
        </div>
      );
    }
  }
}

export default SearchResults;
